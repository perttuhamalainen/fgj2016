﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/*
- when match 100%, go to fade out mode
- level start text
- smoothing of controls
 * 
 * 
 * */
public class GameLevel : MonoBehaviour
{
    enum States
    {
        levelTitle,
        game,
        fadeOut
    };
    States state = States.levelTitle;
    float stateTime = 0;
    int levelIndex = 0;
    const int nPoints=100;
    LineRenderer lr,targetLr;
    List<Transform> dancers=new List<Transform>();
    List<Knob> knobs = new List<Knob>();
    List<HingeJoint> spinJoints = new List<HingeJoint>();
    AudioDistortionFilter dist;
    float audioTime = 0;
    public float masterGain = 0.5f;
    public float targetGain = 0.25f;
    public float baseAudioFrequency = 400.0f;
    public float distortionPumpFrequency = 1.0f;
    public float maxDistortion = 0.8f;
    Camera gameCamera;
    UnityStandardAssets.ImageEffects.BloomOptimized bloom;   
    float[] frequencies;
    public float[] gains = new float[] { 1.0f,0.5f,0.5f,0.5f,0.5f };
    public float[] targetFrequencies = new float[] { 1.0f};
    float[] userWave = new float[nPoints];
    float[] targetWave = new float[nPoints];
    float timeShift = 0;
    Text indicator;
    int synchrony = 0;
    public GameObject dancerTemplate;
    UnityStandardAssets.ImageEffects.NoiseAndGrain noise;
    // Use this for initialization
    void Start()
    {
        stateTime = 0;// Time.timeSinceLevelLoad;
        lr = GameObject.Find("UserLineRenderer").GetComponent<LineRenderer>();
        targetLr = GameObject.Find("TargetLineRenderer").GetComponent<LineRenderer>();
        dist = GetComponent<AudioDistortionFilter>();
        gameCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        indicator = GameObject.Find("Indicator").GetComponent<Text>();
        if (gameCamera == null)
            Debug.LogError("Game camera not found!");
        bloom = gameCamera.GetComponent<UnityStandardAssets.ImageEffects.BloomOptimized>();
        noise = gameCamera.GetComponent<UnityStandardAssets.ImageEffects.NoiseAndGrain>();

        setupLevel(0);
      
    }
    void setupLevel(int idx)
    {
        levelIndex = idx;
        //if played true, loop back to beginning
        if (levelIndex > 4)
            UnityEngine.SceneManagement.SceneManager.LoadScene("startscreen");
        //delete old knobs and dancers
        for (int i=0; i<dancers.Count; i++)
        {
            GameObject.DestroyImmediate(dancers[i].gameObject);
        }
        dancers.Clear();
        for (int i=1; i<dancers.Count; i++)
        {
             GameObject.DestroyImmediate(knobs[i].gameObject);
        }
        knobs.Clear();

        //target frequencies
        if (levelIndex == 0)
            targetFrequencies = new float[] { 1.0f };
        else if (levelIndex == 1)
            targetFrequencies = new float[] { 1.0f, 2.0f };  //dim 5th, perfect fifth
        else if (levelIndex == 2)
            targetFrequencies = new float[] { 1.0f, 3.0f / 2.0f };  //dim 5th, perfect fifth
        else if (levelIndex == 3)
            targetFrequencies = new float[] { 1.0f, 2.0f, 4.0f };  //3.0f/2.0f = perfect fifth
        else if (levelIndex == 4)
            targetFrequencies = new float[] { 1.0f,  1.4046639231824417f, 2.0f};  //dim 5th, perfect fifth
    //        targetFrequencies = new float[] { 1.0f, 3.0f / 2.0f, 1.4046639231824417f, 2.0f };  //dim 5th, perfect fifth
      
        //level names
        string[] levelNames = new string[] { "Level 1: Unity", "Level 2: Double", "Level 3: Overture", "Level 4: Exponential", "Level 5: Dissonance"};
        Text text = GameObject.Find("LevelTitle").GetComponent<Text>();
        text.text = levelNames[levelIndex];

        //find the dancers, depending on the number of frequencies that need to be matched
        frequencies = new float[targetFrequencies.Length];
        for (int i = 0; i < targetFrequencies.Length; i++)        //create more dancers if needed
        {
            GameObject d = (GameObject)GameObject.Instantiate(dancerTemplate,Vector3.zero,Quaternion.identity);
            dancers.Add(d.transform);
        }
        //position the dancers depending on how many there are
        float spacing = 1.5f;
        if (targetFrequencies.Length == 2)
        {
            dancers[1].position = new Vector3(-spacing, 0, 0);
            dancers[0].position = new Vector3(spacing, 0, 0);
        }
        else if (targetFrequencies.Length == 3)
        {
            dancers[2].position = new Vector3(0, 0, 0);
            dancers[1].position = new Vector3(-spacing, 0, 0);
            dancers[0].position = new Vector3(spacing, 0, 0);
        }
        else if (targetFrequencies.Length == 4)
        {
            dancers[0].position = new Vector3(-spacing, 0, -spacing);
            dancers[1].position = new Vector3(spacing, 0, -spacing);
            dancers[2].position = new Vector3(spacing, 0, spacing);
            dancers[3].position = new Vector3(-spacing, 0, spacing);
        }
        else if (targetFrequencies.Length == 5)
        {
            dancers[0].position = new Vector3(0, 0, 0);
            dancers[1].position = new Vector3(-spacing, 0, -spacing);
            dancers[2].position = new Vector3(spacing, 0, -spacing);
            dancers[3].position = new Vector3(spacing, 0, spacing);
            dancers[4].position = new Vector3(-spacing, 0, spacing);
        }
        else if (targetFrequencies.Length > 1)
            Debug.LogError("Too many dancers and frequencies!");
        spinJoints.Clear();

        //update character spinning joint anchors
        for (int i = 0; i < targetFrequencies.Length; i++)        //create more dancers if needed
        {
            Transform hingeBodyTransform = dancers[i].Find("mixamorig:Hips");///mixamorig:LeftUpLeg/mixamorig:LeftLeg");
            HingeJoint j = hingeBodyTransform.gameObject.AddComponent<HingeJoint>();
            j.anchor = Vector3.zero;// dancers[i].position;
            j.axis = Vector3.up;
            spinJoints.Add(j);
        }
        //Kludge: remove old knobs. For some reason orphan knobs linger from previous levels
        while (GameObject.Find("knob1(Clone)") != null)
        {
            GameObject.DestroyImmediate(GameObject.Find("knob1(Clone)"));
        }

        //instantiate UI knobs
        GameObject masterKnob = GameObject.Find("knob1");
        knobs.Add(masterKnob.GetComponent<Knob>());
        Transform parent = masterKnob.transform.parent;
        for (int i = 0; i < targetFrequencies.Length - 1; i++)
        {
            GameObject k = GameObject.Instantiate(masterKnob);
            k.transform.SetParent(parent);
            knobs.Add(k.GetComponent<Knob>());
        }

        updateKnobPositions();
        state = States.levelTitle;
        stateTime = 0;

    }
    void drawCurve(LineRenderer lr, float[] frequencies, float [] out_waveform, float zOffset)
    {
        const float nCycles = 2.0f;
        for (int pt = 0; pt < nPoints; pt++)
        {
            float t = ((float)pt) / ((float)nPoints)*nCycles;
            float val = 0;
            for (int i = 0; i < frequencies.Length; i++)
            {
                val += Mathf.Sin((t+timeShift) * frequencies[i] * Mathf.PI * 2.0f) * gains[i];
            }
            out_waveform[pt] = val;
            Vector3 screenPt = new Vector3(t * Screen.width/nCycles, (0.5f + 0.25f * val) * Screen.height, zOffset);
            Vector3 worldPt = gameCamera.ScreenToWorldPoint(screenPt);
            lr.SetPosition(pt, worldPt);
        }

    }
    void updateKnobPositions()
    {
        //position the knobs
        if (knobs.Count == 1)
        {
            RectTransform r = knobs[0].GetComponent<RectTransform>();
            Vector3 pos = r.localPosition;
            pos.x = 0;
            r.localPosition = pos;
        }
        else
        {
            for (int i = 0; i < knobs.Count; i++)
            {
                RectTransform r = knobs[i].GetComponent<RectTransform>();
                Vector3 pos = r.localPosition;
                float rel = ((float)i) / (float)(targetFrequencies.Length - 1);
                rel -= 0.5f;
                pos.x = Screen.width * 0.4f * rel;
                r.localPosition = pos;
            }
        }
    }
    float audioFadeGain = 0.0f;
    void Update()
    {
        stateTime += Time.deltaTime;
        if (state == States.levelTitle)
            update_title();
        else if (state == States.game)
            update_game();
        else if (state == States.fadeOut)
            update_fadeout();
        Image fadeImage = GameObject.Find("FadeImage").GetComponent<Image>();
        audioFadeGain = 1.0f-fadeImage.color.a;
    }
    void update_title()
    {
        Text text = GameObject.Find("LevelTitle").GetComponent<Text>();
        Image fadeImage=GameObject.Find("FadeImage").GetComponent<Image>();
        const float fadeTime = 1.0f;
        const float titleShowTime = 1.0f;
        if (stateTime < fadeTime+titleShowTime)
        {
            fadeImage.color=Color.black;
            Color color=text.color;
            color.a=Mathf.Clamp01(stateTime);
            text.color=color;
        }
        else if ((stateTime > fadeTime + titleShowTime) && (stateTime < fadeTime*2.0f + titleShowTime))
        {
            fadeImage.color = new Color(0, 0, 0, Mathf.Clamp01(1.0f - (stateTime - fadeTime - titleShowTime)));
            Color color = text.color;
            color.a = Mathf.Clamp01(1.0f - (stateTime - fadeTime - titleShowTime));
            text.color = color;
            update_game();  //game becoming visible
        }
        else
        {
            stateTime=0;
            state=States.game;
        }
    }
    void update_fadeout()
    {
        update_game();
        Image fadeImage = GameObject.Find("FadeImage").GetComponent<Image>();
        const float fadeOutTime=5.0f;
        fadeImage.color = new Color(0, 0, 0, Mathf.Clamp01(stateTime / fadeOutTime));
        Light light = GameObject.Find("Spotlight").GetComponent<Light>();
        light.intensity = 1.3f + 8.0f * Mathf.Pow(Mathf.Clamp01(stateTime / fadeOutTime),3.0f);
        if (stateTime > fadeOutTime)
        {
            state = States.levelTitle;
            stateTime = 0;
            for (int i = 0; i < frequencies.Length; i++)
            {
                frequencies[i] = 0;
            }
            for (int i = 0; i < knobs.Count; i++)
            {
                knobs[i].reset();
            }
            setupLevel(levelIndex + 1);
            light.intensity = 1.3f;
        }
    }
    // Update is called once per frame
    void update_game()
    {
        
     //   updateKnobPositions();
        //timeShift += 0.001f;
        //update frequencies based on knobs
        if (state == States.game)
        {
            for (int i = 0; i < knobs.Count; i++)
            {
                frequencies[i] = knobs[i].value * 8.0f;
            }
        }
        else if (state == States.fadeOut)
        {
            //if level complete, sweep the user frequencies towards target to achieve perfect harmony
            for (int i = 0; i < targetFrequencies.Length; i++)
            {
                frequencies[i] = 0.9f*frequencies[i]+0.1f*targetFrequencies[i];
            }
        }

        //update line renderers
        drawCurve(lr, frequencies,userWave,1.0f);
        drawCurve(targetLr, targetFrequencies,targetWave,1.1f);

        //compute match
        float err = 0;
        for (int i = 0; i < nPoints; i++)
        {
            err += Mathf.Abs(userWave[i] - targetWave[i]);
        }
        synchrony = (int)Mathf.Clamp(120.0f - err, 0, 100);
        if (state == States.game)
        {
            indicator.text = "Match: " + synchrony + "%";
            if (synchrony == 100)
            {
                stateTime = 0;
                state = States.fadeOut;
                indicator.text = "Transcending...";
            }
        }

        //make dancers spin
        const float baseVisualFrequency = 1.0f;
        for (int i = 0; i < dancers.Count; i++)
        {
            JointMotor motor = spinJoints[i].motor;
            spinJoints[i].useMotor = true;
            motor.targetVelocity = frequencies[i] * baseVisualFrequency * 360.0f;
            motor.force = 1000;
            spinJoints[i].motor = motor;
            //rb.MoveRotation(Quaternion.AngleAxis(frequencies[i] * baseVisualFrequency * 360.0f * Time.timeSinceLevelLoad, Vector3.up));
            //rb.MoveRotation(Quaternion.AngleAxis(45,Vector3.forward));
//            Transform hipTransform = dancers[i].Find("mixamorig:Hips");
           // Rigidbody rb = hipTransform.GetComponent<Rigidbody>();
            //rb.AddTorque(new Vector3(0, frequencies[i] * baseVisualFrequency * 360.0f, 0), ForceMode.VelocityChange);
         //   rb.angularVelocity = new Vector3(0, frequencies[i] * baseVisualFrequency * 360.0f, 0);
           // rb.MoveRotation(Quaternion.AngleAxis(frequencies[i] * baseVisualFrequency * 360.0f * Time.deltaTime, Vector3.up)*rb.rotation);
            //hipTransform.Rotate(0, frequencies[i] * baseVisualFrequency * 360.0f * Time.deltaTime, 0);
            //hipTransform = hipTransform.Find("mixamorig:LeftUpLeg");
            //hipTransform.Rotate(0, frequencies[i] * baseVisualFrequency * 360.0f * Time.deltaTime, 0);
            //hipTransform = hipTransform.Find("mixamorig:LeftLeg");
            //hipTransform.Rotate(0, frequencies[i] * baseVisualFrequency * 360.0f * Time.deltaTime, 0);
            ////Transform hipTransform = dancers[i].Find("mixamorig:Hips");
        }

        //distortion pump
        dist.distortionLevel = maxDistortion * 0.5f * (1.0f + Mathf.Sin(Mathf.PI * 2.0f * Time.timeSinceLevelLoad * distortionPumpFrequency));

        //bloom pump, sync with distortion
        const float bloomMinIntensity=0.9f;
        const float bloomMaxIntensity=1.5f;
        const float bloomMinBlur=1.0f;
        const float bloomMaxBlur=2.0f;
        float rel=0.5f * (1.0f + Mathf.Sin(Mathf.PI * 2.0f * Time.timeSinceLevelLoad * distortionPumpFrequency));
        bloom.blurSize = bloomMinBlur + rel * (bloomMaxBlur - bloomMinBlur);
        bloom.intensity = bloomMinIntensity + rel * (bloomMaxIntensity - bloomMinIntensity);

        //noise pump
        noise.intensityMultiplier = 2.0f + rel * 2.0f;
    }
   
    void OnGUI()
    {
    }
    void OnAudioFilterRead(float[] data, int channels)
    {
        for (int i = 0; i < data.Length; i += channels)
        {
            audioTime += 1.0f / 44100.0f;   //assuming 44k sampling rate
            float val = 0;
            //user sound
            for (int j = 0; j < frequencies.Length; j++)
            {
                val += Mathf.Sin(audioTime * frequencies[j] * baseAudioFrequency * Mathf.PI * 2.0f) * gains[j] * masterGain*audioFadeGain;
            }
            //target sound  (only play base frequency to avoid aural clutter but have some hint for the player)
            for (int j = 0; j < 1; j++)
            {
                val += Mathf.Sin(audioTime * targetFrequencies[j] * baseAudioFrequency * Mathf.PI * 2.0f) * gains[j] * masterGain*audioFadeGain *targetGain;
            }
            //add to the data
            for (int c = 0; c < channels; c++)
            {
                data[i + c] += val;
            }
        }
    }
}
