﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SineTest : MonoBehaviour {
    LineRenderer lr;
    public List<Transform> dancers;
    AudioDistortionFilter dist;
    float audioTime = 0;
    public float masterGain = 0.5f;
    public float baseAudioFrequency = 400.0f;
    public float distortionPumpFrequency = 1.0f;
    public float maxDistortion = 0.8f;
    // Use this for initialization
	void Start () {
        lr = GetComponent<LineRenderer>();
        dist = GetComponent<AudioDistortionFilter>();
	}
	
	// Update is called once per frame
	void Update () {
        for (int pt=0; pt<100; pt++)
        {
            float t=((float)pt)/100.0f;
            float val=0;
            for (int i=0; i<nComponents; i++)
            {
                val+=Mathf.Sin(t*frequencies[i]*Mathf.PI*2.0f)*gains[i];
            }
            lr.SetPosition(pt, new Vector3(t*5.0f, val,0));
        }
        const float baseVisualFrequency=0.2f;
        for (int i = 0; i < nComponents; i++)
        {
            dancers[i].transform.Rotate(0, frequencies[i] * baseVisualFrequency * 360.0f * Time.deltaTime, 0);
        }

        //distortion pump
        dist.distortionLevel = maxDistortion * 0.5f * (1.0f + Mathf.Sin(Mathf.PI * 2.0f * Time.timeSinceLevelLoad * distortionPumpFrequency));
	}
    const int nComponents = 4;
    float[] frequencies = new float[nComponents]{1.0f,1.0f,1.0f,1.0f};
    float[] gains = new float[nComponents] { 1.0f, 0.5f, 0.25f, 0.1f };
    void OnGUI()
    {
        for (int i=0; i<nComponents; i++)
        {
            frequencies[i]=GUI.HorizontalSlider(new Rect(10,i*20,200,20),frequencies[i],1,16);
        }
    }
    void OnAudioFilterRead(float[] data, int channels)
    {
        for (int i = 0; i < data.Length; i+=channels)
        {
            audioTime += 1.0f / 44100.0f;   //assuming 44k sampling rate
            float val = 0;
            for (int j = 0; j < nComponents; j++)
            {
                val += Mathf.Sin(audioTime * frequencies[j] * baseAudioFrequency * Mathf.PI * 2.0f) *gains[j]*masterGain;
            }
            for (int c = 0; c < channels; c++)
            {
                data[i + c] += val;
            }
        }
	}
}
