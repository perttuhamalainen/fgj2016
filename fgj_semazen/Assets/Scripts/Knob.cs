﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Knob : MonoBehaviour {
    RectTransform rt;
    public float value=0;
    float rotateSpeed = 0.025f;

	// Use this for initialization
	void Start () {
        rt = GetComponent<RectTransform>();
        oldMousePos = Input.mousePosition;
	}
    public void reset()
    {
        rt.rotation = Quaternion.identity;
        value = 0;
    }
	// Update is called once per frame
	void Update () {
        mouseDelta = Input.mousePosition - oldMousePos;
        oldMousePos = Input.mousePosition;
	}
    public void OnPointerEnter(Object obj)
    {
    }
    Vector3 oldMousePos;
    Vector3 mouseDelta;
    public void OnDrag(Object obj)
    {
//        value = Mathf.Clamp(value + rotateSpeed*(mouseDelta.y), 0, 1);
        value = Mathf.Clamp(value + rotateSpeed * (Input.GetAxis("Mouse Y")), 0, 1);
        rt.rotation = Quaternion.Euler(new Vector3(0, 0, -value * 280.0f));
    }
}
