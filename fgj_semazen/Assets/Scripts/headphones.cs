﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class headphones : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.timeSinceLevelLoad < 2.0f)
            GetComponent<Image>().color = new Color(1, 1, 1, Mathf.Clamp01(Time.timeSinceLevelLoad));
        else
            GetComponent<Image>().color = new Color(1, 1, 1, Mathf.Clamp01(3.0f-Time.timeSinceLevelLoad));
        if (Time.timeSinceLevelLoad > 3.0f)
            UnityEngine.SceneManagement.SceneManager.LoadScene("startscreen");

	}
}
