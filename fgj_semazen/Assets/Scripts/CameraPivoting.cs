﻿using UnityEngine;
using System.Collections;

public class CameraPivoting : MonoBehaviour {
    public float xFreq = 0.233f;
    public float yFreq = 0.5f;
    public float zFreq = 0.11f;
    public float xzRange = 5.0f;
    public float yMin = 2.0f;
    public float yMax = 5.0f;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(Mathf.Sin(Time.timeSinceLevelLoad * xFreq * 2 * Mathf.PI) * xzRange,
            yMin + (yMax - yMin) * (0.5f + 0.5f * Mathf.Sin(Time.timeSinceLevelLoad * yFreq * 2 * Mathf.PI)),
            Mathf.Cos(Time.timeSinceLevelLoad * xFreq * 2 * Mathf.PI) * xzRange);
        //transform.position = new Vector3(Mathf.Sin(Time.timeSinceLevelLoad * xFreq * 2 * Mathf.PI) * xzRange,
        //    yMin + (yMax - yMin) * (0.5f + 0.5f * Mathf.Sin(Time.timeSinceLevelLoad * yFreq * 2 * Mathf.PI)),
        //    Mathf.Cos(Time.timeSinceLevelLoad * zFreq * 2 * Mathf.PI) * xzRange);
        transform.LookAt(new Vector3(0, 1.0f, 0));
	}
}
