﻿using UnityEngine;
using System.Collections;


public class text : MonoBehaviour {
	public float maxsize = 1.5f;
	public float minsize = 0.75f;
	public float scalespd = 0.01f;
	public float scalespdy = 0.005f;
	//private float alphaspd = 1 / scalespd;
	private bool grow = true;
	private bool pulsate = false;
	private UnityEngine.UI.Text txt;


	// Use this for initialization
	void Start () {
		 txt = gameObject.GetComponent<UnityEngine.UI.Text>();
	}

	public void Respond (Object o)
	{
		pulsate = true;
	}

	public void Respond2 (Object o) 
	{
		pulsate = false;

		transform.localScale = new Vector3 (1.0f, 1.0f, 0.0f); 
		Color a = txt.color;
		a.a = 1.0f;
		txt.color = a;
	}

	// Update is called once per frame
	void Update() {
		if (pulsate == true) {
			if (grow == true) {
				if (transform.lossyScale.x < maxsize) {
					transform.localScale += new Vector3 (scalespd, scalespdy, 0.0f);

					Color a = txt.color;
					a.a -= 0.005f;
					txt.color = a;
				} else {
					grow = false;
				}
			} else {
				if (transform.lossyScale.x > minsize) {
					transform.localScale += new Vector3 (-scalespd, -scalespdy, 0.0f);

					Color a = txt.color;
					a.a += 0.005f;
					txt.color = a;
				} else {
					grow = true;
				}
			}
		} /*else 
		{
			transform.localScale = new Vector3 (1.0f, 1.0f, 0.0f); 
			Color a = txt.color;
			a.a = 1.0f;
			txt.color = a;

		}*/
	}
}
