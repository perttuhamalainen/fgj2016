﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;



public class fader : MonoBehaviour {
	private Image img;
	private Button btn;
	private Color clr;
	private bool fadedone;
	Color targetcolor,startColor;
	float fadestarttime;
	// Use this for initialization
	void Start () {
		img = gameObject.GetComponent<Image> ();
		clr = img.color;
		startFadein ();
	}
	void startFadein()
	{
		startColor = img.color;
		targetcolor = new Color (0.0f, 0.0f, 0.0f, 0.0f);
		fadestarttime = Time.timeSinceLevelLoad;
	}

	public void startFadeout()
	{
		startColor = img.color;
		targetcolor = new Color (0.0f, 0.0f, 0.0f, 1.0f);
		fadestarttime = Time.timeSinceLevelLoad;
	}
	// Update is called once per frame
	void Update () {
		img.color = Color.Lerp (startColor, targetcolor, Mathf.Clamp01 (Time.timeSinceLevelLoad - fadestarttime));

        GameObject.Find("Plane").GetComponent<AudioSource>().volume = 1.0f-img.color.a;
	}
}
